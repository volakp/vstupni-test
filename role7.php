<?php
while (list($nazev,$hodnota) = each($_POST)){
	$$nazev= htmlspecialchars($hodnota, ENT_QUOTES, 'UTF-8');;
}
?>

<!DOCTYPE html  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2" />
  <meta http-equiv="Content-Language" content="cs" />
  <style> label {min-width:290px; display:inline-block}</style>
</head>
<body>
  <form action="role7.php" method="post">
    <input type="hidden" name="test" value=1>
    <label for="pole">Zadejte seznam slov oddělené pomlčkou:</label>
    <input type="text" name="pole" id="pole" placeholder="slovo1 - slovo2 -.." value="<?php echo empty($pole) ? "" : $pole;?>">
    <br />
    <label for="sl1">Zadejte první hledané slovo:</label>
    <input type="text" name="sl1" id="sl1" placeholder="slovo1" value="<?php echo empty($sl1) ? "" : $sl1;?>">
    <br />
    <label for="sl2">Zadejte druhé hledané slovo:</label>
    <input type="text" name="sl2" id="sl1" placeholder="slovo2" value="<?php echo empty($sl2) ? "" : $sl2;?>">
    <br />
    <input type="submit" value="Najdi pozici">
  </form>
  <div>
  <?php
  if( !empty($test)){
    if( !empty($pole)){
      if( !empty($sl1)){
        $sl1 = strtolower( trim( $sl1));
        if( !empty($sl2)){ 
          $sl2 = strtolower( trim( $sl2));
          $pol = explode("-", strtolower( $pole));
          if( count( $pol) < 1){
            echo "V seznamu je málo slov";
          }else{
            $ps1 = PHP_INT_MAX; $ps2 = PHP_INT_MAX;
            for( $i=0; $i<count( $pol); $i++)  {
              if( (trim( $pol[$i]) == $sl1) and ($i < $ps1)){ $ps1 = $i;}
              if( (trim( $pol[$i]) == $sl2) and ($i < $ps2)){ $ps2 = $i;}
            }
            if( ($ps1 == PHP_INT_MAX) or ($ps2 == PHP_INT_MAX)){
              echo "Nelze určit vzdalenost";
            }else{
              echo "Minimalní rozdíl mezi $sl1 a $sl2 je ". abs($ps1 - $ps2);
            }
          }
        }else{
          echo "Nebylo zadáno druhé slovo";    
          echo "<script>document.getElementById('sl2').focus();</script>";
        }
      }else{
        echo "Nebylo zadáno první slovo";    
        echo "<script>document.getElementById('sl1').focus();</script>";
      }
    }else{
      echo "Nebyl zadán seznam slov";    
      echo "<script>document.getElementById('pole').focus();</script>";
    }
  }
  ?>
  </div>
</body>
</html>  
<?php

function souborNaDisku( $path){
  echo "<ul>";
  $adres = array_diff(scandir($path), array('.', '..'));
  foreach ($adres as &$file) {
    echo "<li>".$file."</li>";
    if( is_dir($path . $file)){
      souborNaDisku( $path.$file. DIRECTORY_SEPARATOR);
    }
  }
  echo "</ul>";
}
?>