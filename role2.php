<?php
while (list($nazev,$val) = each($_POST)){
	$$nazev= htmlspecialchars($val, ENT_QUOTES, 'UTF-8');;
}
?>

<!DOCTYPE html  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2" />
   <meta http-equiv="Content-Language" content="cs" />
</head>
<body>
  <form action="role2.php" method="post">
  <input type="hidden" name="test" value=1>
  <label for="slovnik">Zadejte řetězcový slovník:</label>
    <input type="text" name="slovnik" id="slovnik" placeholder="ale, apple, monkey, plea,.." value="<?php echo empty($slovnik) ? "" : $slovnik;?>">
    <br />
    <label for="retez">Zadejte řetězec znaků:</label>
    <input type="text" name="retez" id="retez" placeholder="apxplmcweeya" value="<?php echo empty($retez) ? "" : $retez?>">
    <br />
    <input type="submit" value="Proved hledaní">
  </form>
  <div>
  <?php
  if( !empty($test)){
    if( !empty( $slovnik)){
      if( !empty( $retez)){
        $slovnik = explode(",", $slovnik);
        do{
          $sort = true;
          for( $i=0; $i<count( $slovnik)-1; $i++)  {
            if( strlen( trim( $slovnik[$i]))< strlen( trim( $slovnik[$i+1]))){
              $p = trim( $slovnik[$i]);
              $slovnik[$i] = trim( $slovnik[$i+1]);
              $slovnik[$i+1] = $p;
              $sort = false;
            }
          }
        }while ($sort==false);
        $result = array();
        foreach ($slovnik as &$slovo) {  
          $pom = "";
          $store = $retez;
          for ($i=0; $i < strlen( $slovo) ; $i++) { 
            if( stripos(  $store, $slovo[$i]) !== false){
              $pom.= $slovo[$i];
              $p= stripos( $store, $slovo[$i]);
              if( $slovo == $pom){
                array_push($result, $slovo); 
                break;
              }
              $store = substr( $store, 0, $p).substr( $store, $p+1, strlen($store)-1);
            }
          }
        }
        if ( !empty( $result)){
          $length = strlen( $result[0]);
          echo "Byla nalezena schoda: ";
          foreach ($result as &$slovo) {  
            if( strlen( $slovo) < $length){return;}
            echo $slovo.", ";
            $length = strlen( $slovo);
          }
        }else{
          echo "Nebyla nalezena schoda";
        }
      }else{
        echo "Nebyl zadán řetězec znaků";    
        echo "<script>document.getElementById('retez').focus();</script>";
      }
    }else{
      echo "Nebyl zadán řetězcový slovník";    
      echo "<script>document.getElementById('slovnik').focus();</script>";
    }
  }
  ?>
  </div>
</body>
</html>  
